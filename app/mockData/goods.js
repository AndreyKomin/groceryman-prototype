const goods = [{
  'id': '1',
  'title': 'Молоко детское Агуша витаминизированное 2.5% 200мл',
  'description': 'Молоко детское Агуша витаминизированное 2.5% 200мл производится из нормализованного молока, обогащенного целым витаминным премиксом, необходимым растущему организму. Он содержит витамин А - для зрения, В1 - для нервной системы, В2 - для обмена веществ, С - для иммунитета и йод - для умственной деятельности. Рекомендуется для питания детей с 8 месяцев. Перед употреблением молоко следует прогреть до комфортной температуры. Продукт можно использовать для приготовления каш, омлетов и других блюд для вашего малыша. Обратите внимание: перед введением нового продукта в рацион ребенка обязательно проконсультируйтесь с вашим педиатром!',
  'priceRouble': '27',
  'pricePenny': '90',
  'units': '/шт',
  'cover': '/src/product.file/full/image/26/60/26026.jpeg',
  'count': 1
}, {
  'id': '2',
  'title': 'Молоко Простоквашино Отборное 3.4-4.5% 930мл',
  'description': 'Молоко Простоквашино Отборное 3.4-4.5% 930мл - цельное пастеризованное молоко традиционной жирности. При пастеризации свежее коровье молоко сохраняет большую часть полезных составляющих, но при этом не избавляется полностью от микробов и вредных бактерий, поэтому и срок хранения у него не очень большой. Такое молоко - хороший источник полноценного белка, витаминов группы B, аминокислот, кальция и фосфора. Вся продукция торговой марки имеет несколько гарантий качества: она готовится из 100% натурального молока с ближайших ферм, проверяется на наличие антибиотиков, не содержит консервантов и растительных жиров.',
  'priceRouble': '71',
  'pricePenny': '00',
  'units': '/шт',
  'cover': '/src/product.file/full/image/13/63/26313.jpeg',
  'count': 1
}, {
  'id': '3',
  'title': 'Молоко Нашей Дойки 3.4-6% 1л',
  'priceRouble': '73',
  'pricePenny': '00',
  'units': '/шт',
  'cover': '/src/product.file/full/image/41/96/9641.jpeg',
  'count': 1
}, {
  'id': '4',
  'title': 'Молоко Правильное Молоко 3.2-4% 900мл',
  'priceRouble': '79',
  'pricePenny': '90',
  'units': '/шт',
  'cover': '/src/product.file/full/image/19/26/22619.jpeg',
  'count': 1
}, {
  'id': '5',
  'title': 'Молоко Parmalat Natura Premium 3.5% 1л',
  'description': 'Молоко Parmalat Natura Premium 3.5% 1л - это классический напиток, приготовленный по европейским стандартам качества из отборного натурального молока без добавления сухого молока, красителей и консервантов. Благодаря ультрапастеризации продукт сохранил все витамины и микроэлементы (в том числе кальций, фосфор и витамин D), которыми так славится свежее коровье молоко, при этом он может храниться дольше пастеризованного. Расфасован в герметичный пакет с отвинчивающейся крышечкой.',
  'priceRouble': '78',
  'pricePenny': '00',
  'units': '/шт',
  'cover': '/src/product.file/full/image/94/26/22694.jpeg',
  'count': 1
}, {
  'id': '6',
  'title': 'Молоко Домик в деревне 6% 928мл',
  'description': 'Молоко Домик в деревне 6% 928мл - продукт повышенной жирности, приготовленный из цельного молока с добавлением сливок. Ультрапастеризация, особый способ переработки, позволяет сохранить все полезные качества свежего коровьего молока, его вкус и витаминно-минеральный состав. Молоко высокой жирности - не только хороший источник полноценного белка, витаминов и минералов, но и незаменимый продукт в кулинарии. У него более сливочный вкус, поэтому даже привычные блюда получаются нежнее и вкуснее. Упаковано в пакет Tetra Pak с крышечкой.',
  'priceRouble': '99',
  'pricePenny': '90',
  'units': '/шт',
  'cover': '/src/product.file/full/image/77/26/22677.jpeg',
  'count': 1
}, {
  'id': '7',
  'title': 'Молоко Домик в деревне 0.5% 923мл',
  'description': 'Молоко Домик в деревне 0.5% 923мл - легкий диетический продукт, приготовленный из цельного и обезжиренного молока с проверенных молочных ферм. Ультрапастеризация, особый способ переработки, позволяет сохранить все полезные качества свежего коровьего молока, его вкус и витаминно-минеральный состав. Молоко с минимальной жирностью - это низкокалорийный источник полноценного белка, витаминов группы B, аминокислот, кальция и фосфора. Это отличный продукт для тех, кто следит за фигурой и придерживается диетического или спортивного питания. Упаковано в пакет Tetra Pak с крышечкой.',
  'priceRouble': '78',
  'pricePenny': '00',
  'units': '/шт',
  'cover': '/src/product.file/full/image/58/03/20358.jpeg',
  'count': 1
}, {
  'id': '8',
  'title': 'Коктейль молочный Чудо Шоколад 3% 200мл',
  'description': 'Коктейль молочный Чудо Шоколад 3% 200мл - это настоящее шоколадное лакомство, приготовленное на основе натурального молока с добавлением какао-порошка и темного шоколада. Напиток имеет насыщенный шоколадный цвет, аромат и, конечно же, вкус, при этом в нем сохранилась вся польза и ценность молока. Поэтому даже если ваш ребенок молоко не любит, от такого коктейля он вряд ли откажется. Маленькую упаковку с трубочкой удобно брать с собой в дорогу или на работу, давать ребенку в школу или на прогулку.',
  'priceRouble': '24',
  'pricePenny': '00',
  'units': '/шт',
  'cover': '/src/product.file/full/image/50/46/24650.jpeg',
  'count': 1
}, {
  'id': '9',
  'title': 'Молоко Домик в деревне 3.5% 925мл',
  'description': 'Молоко Домик в деревне 3.5% 925мл - продукт традиционно средней жирности, приготовленный из нормализованного молока с проверенных молочных ферм. Ультрапастеризация, особый способ переработки, позволяет сохранить все полезные качества свежего коровьего молока, его вкус и витаминно-минеральный состав. Такое молоко - хороший источник полноценного белка, витаминов группы B, аминокислот, кальция и фосфора. Это важный продукт в ежедневном рационе каждого члена семьи и незаменимый - в кулинарии. Упаковано в пакет Tetra Pak с крышечкой.',
  'priceRouble': '89',
  'pricePenny': '90',
  'units': '/шт',
  'cover': '/src/product.file/full/image/41/03/20341.jpeg',
  'count': 1
}, {
  'id': '10',
  'title': 'Молоко Домик в деревне 3.5% 200мл',
  'description': 'Молоко Домик в деревне 3.5% 200мл - продукт традиционно средней жирности, приготовленный из цельного молока с добавлением сливок. Ультрапастеризация, особый способ переработки, позволяет сохранить все полезные качества свежего коровьего молока, его вкус и витаминно-минеральный состав. Такое молоко - хороший источник полноценного белка, витаминов группы B, аминокислот, кальция и фосфора. Подходит для питания детей старше 3 лет. Упаковано в пакетик Tetra Pak, есть трубочка для питья. Удобно брать с собой в дорогу, давать ребенку в школу или на прогулку.',
  'priceRouble': '24',
  'pricePenny': '90',
  'units': '/шт',
  'cover': '/src/product.file/full/image/62/07/20762.jpeg',
  'count': 1
}, {
  'id': '11',
  'title': 'Молоко детское Тема 3.2% 200мл',
  'description': 'Молоко детское Тема ультрапастеризованное 3.2% 200мл - это готовый к употреблению продукт, изготовленный из цельного и обезжиренного молока. Метод ультрапастеризации (кратковременный нагрев до высоких температур и последующее резкое охлаждение) позволяет уничтожить все бактерии, при этом сохранить максимальное количество полезных веществ. Рекомендуется для питания детей с 12 месяцев. Перед употреблением следует прогреть на водяной бане до 37 градусов. Подходит для приготовления каш, омлетов и других блюд для детского питания. Обратите внимание: перед введением нового продукта в рацион ребенка обязательно проконсультируйтесь с вашим педиатром!',
  'priceRouble': '27',
  'pricePenny': '90',
  'units': '/шт',
  'cover': '/src/product.file/full/image/52/95/19552.jpeg',
  'count': 1
}, {
  'id': '12',
  'title': 'Молоко М Лианозовское 3.2% 925мл',
  'priceRouble': '58',
  'pricePenny': '00',
  'units': '/шт',
  'cover': '/src/product.file/full/image/57/26/22657.jpeg',
  'count': 1
}, {
  'id': '13',
  'title': 'Молоко детское Тема 3.2% 500мл',
  'description': 'Молоко детское Тема ультрапастеризованное 3.2% 500мл - это готовый к употреблению продукт, изготовленный из цельного и обезжиренного молока. Метод ультрапастеризации (кратковременный нагрев до высоких температур и последующее резкое охлаждение) позволяет уничтожить все бактерии, при этом сохранить максимальное количество полезных веществ. Рекомендуется для питания подросших детей с 3 лет. Перед употреблением следует прогреть на водяной бане до 37 градусов. Подходит для приготовления каш, омлетов и других блюд для детского питания. Обратите внимание: перед введением нового продукта в рацион ребенка обязательно проконсультируйтесь с вашим педиатром!',
  'priceRouble': '48',
  'pricePenny': '00',
  'units': '/шт',
  'cover': '/src/product.file/full/image/43/19/21943.jpeg',
  'count': 1
}, {
  'id': '14',
  'title': 'Коктейль молочный Растишка Банан-клубника 210мл',
  'description': 'Коктейль молочный Растишка Банан-клубника 210мл - это полезный нежирный напиток с фруктово-ягодным ароматом, рекомендуемый для питания детей с 3 лет. Он производится из цельного и обезжиренного молока с добавлением витамина D3 и целого комплекса минеральных веществ, необходимых для здорового роста и развития малыша. Он может заменить обычное молоко во время завтрака или полдника. Упаковку с соломинкой удобно брать в дорогу, давать ребенку в школу или на прогулку. Обратите внимание: перед введением нового продукта в рацион обязательно проконсультируйтесь с вашим педиатром!',
  'priceRouble': '32',
  'pricePenny': '90',
  'units': '/шт',
  'cover': '/src/product.file/full/image/96/86/38696.jpeg',
  'count': 1
}, {
  'id': '15',
  'title': 'Молоко Асеньевская ферма 3.4-6% 900мл',
  'description': 'Молоко Асеньевская ферма 3.4-6% 900мл - это 100% натуральный органический напиток, приготовленный из цельного коровьего молока и прошедший минимальную обработку после дойки. Пастеризация, особый способ переработки, позволяет сохранить все полезные свойства свежего молока, его вкус и витаминно-минеральный состав. Молоко - хороший источник полноценного белка, витаминов группы B, аминокислот, кальция и фосфора. Не содержит ГМО, искусственные добавки и сухое молоко. Упаковано в пластиковую бутылку с крышечкой.',
  'priceRouble': '79',
  'pricePenny': '90',
  'units': '/шт',
  'cover': '/src/product.file/full/image/90/25/22590.jpeg',
  'count': 1
}, {
  'id': '16',
  'title': 'Коктейль молочный Растишка Клубника-земляника 210мл',
  'description': 'Коктейль молочный Растишка Клубника-земляника 210мл - это полезный нежирный напиток с ягодным ароматом, рекомендуемый для питания детей с 3 лет. Он производится из цельного и обезжиренного молока с добавлением витамина D3 и целого комплекса минеральных веществ, необходимых для здорового роста и развития малыша. Он может заменить обычное молоко во время завтрака или полдника. Упаковку с соломинкой удобно брать в дорогу, давать ребенку в школу или на прогулку. Обратите внимание: перед введением нового продукта в рацион обязательно проконсультируйтесь с вашим педиатром!',
  'priceRouble': '32',
  'pricePenny': '90',
  'units': '/шт',
  'cover': '/src/product.file/full/image/35/46/24635.jpeg',
  'count': 1
}, {
  'id': '17',
  'title': 'Молоко Народное 3.2% 900мл',
  'priceRouble': '44',
  'pricePenny': '90',
  'units': '/шт',
  'cover': '/src/product.file/full/image/12/47/24712.jpeg',
  'count': 1
}, {
  'id': '18',
  'title': 'Молоко Молочное царство 3.2% 900мл',
  'priceRouble': '31',
  'pricePenny': '00',
  'units': '/шт',
  'cover': '/src/product.file/full/image/34/08/20834.jpeg',
  'count': 1
}, {
  'id': '19',
  'title': 'Молоко 36 Копеек 3.2% 873мл',
  'description': 'Молоко 36 Копеек пастеризованное 3.2% 873мл - легендарный продукт Останкинского молочного комбината, запущенный в производство к московской Олимпиаде 1980 года. Именно тогда был придуман узнаваемый логотип бренда и его знаменитый дизайн с колосками. Как и несколько десятилетий назад, напиток фасуется в картонный пакет с гребешком, только теперь он имеет удобную отвинчивающуюся крышку. Это молоко пастеризованное, в нем сохранилась большая часть полезных микроэлементов и витаминов, однако срок хранения у него небольшой - всего несколько дней.',
  'priceRouble': '51',
  'pricePenny': '00',
  'units': '/шт',
  'cover': '/src/product.file/full/image/48/03/20348.jpeg',
  'count': 1
}, {
  'id': '20',
  'title': 'Молоко Пастушок 3.2% 925мл',
  'description': 'Молоко Пастушок 3.2% 925мл производится из нормализованного коровьего молока методом ультрапастеризации. Такой способ обработки позволяет сохранить все вкусовые и полезные качества напитка, а также продлить срок его годности. Пейте его охлажденным или теплым, используйте для приготовления каш и омлетов, выпечки и соусов, нежного картофельного пюре и поленты, муссов и желе. Продукт не содержит сухого молока, при взбивании образует пышную пенку и подходит для приготовления молочных коктейлей и кофейных напитков.',
  'priceRouble': '66',
  'pricePenny': '90',
  'units': '/шт',
  'cover': '/src/product.file/full/image/87/26/22687.jpeg',
  'count': 1
}, {
  'id': '21',
  'title': 'Молоко Сарафаново 3.2% 950мл',
  'priceRouble': '69',
  'pricePenny': '90',
  'units': '/шт',
  'cover': '/src/product.file/full/image/58/96/29658.jpeg',
  'count': 1
}, {
  'id': '22',
  'title': 'Молоко Белый город 3.2% 1л',
  'description': 'Молоко Белый город 3.2% 1л - это продукт Белгородского молочного комбината, крупнейшего производителя молочной продукции в Центральном Черноземье. Производится из высококачественного коровьего молока методом ультрапастеризации. Такой способ обработки позволяет сохранить все вкусовые и полезные качества напитка, а также продлить срок его годности. Асептическая упаковка создает надежную защиту от света, кислорода и бактерий. После вскрытия пакет рекомендуется хранить в холодильнике.',
  'priceRouble': '64',
  'pricePenny': '90',
  'units': '/шт',
  'cover': '/src/product.file/full/image/41/19/21941.jpeg',
  'count': 1
}, {
  'id': '23',
  'title': 'Молоко Parmalat Natura Premium 0.5% 1л',
  'description': 'Молоко Parmalat Natura Premium 0.5% 1л - это напиток минимальной жирности, приготовленный по европейским стандартам качества из отборного натурального молока без добавления сухого молока, красителей и консервантов. Благодаря ультрапастеризации продукт сохранил все витамины и микроэлементы (в том числе кальций, фосфор и витамин D), которыми так славится свежее коровье молоко, при этом он может храниться дольше пастеризованного. Подходит для диетического питания. Расфасован в герметичный пакет с отвинчивающейся крышечкой.',
  'priceRouble': '62',
  'pricePenny': '90',
  'units': '/шт',
  'cover': '/src/product.file/full/image/90/26/22690.jpeg',
  'count': 1
}, {
  'id': '24',
  'title': 'Молоко Афанасий Живое 3.2% 900мл',
  'description': 'Молоко Афанасий Живое Десятисуточное 3.2% 900мл - это 100% натуральный продукт, который производится из цельного и обезжиренного молока с частных ферм Тверской области. Прошедшее пастеризацию (минимальную термическую обработку) молоко сохраняет все свои полезные свойства и является ценным источником кальция, магния, фосфора и калия. Не содержит антибиотиков, пестицидов, сухого молока, консервантов и искусственных добавок. При комнатной температуре начинает скисать и подходит для приготовления простокваши или творога.',
  'priceRouble': '64',
  'pricePenny': '90',
  'units': '/шт',
  'cover': '/src/product.file/full/image/75/73/37375.jpeg',
  'count': 1
}]

export default goods

const localshops = [
  {
    id: 1,
    name: 'Монетка',
    image: 'https://firebasestorage.googleapis.com/v0/b/groceryman-5b279.appspot.com/o/logos%2Fmonetka.png?alt=media&token=3a866be7-ad54-48bd-a60f-f08b59da6fc3'
  },
  {
    id: 2,
    name: 'Перекресток',
    image: 'https://firebasestorage.googleapis.com/v0/b/groceryman-5b279.appspot.com/o/logos%2Flogo-perekrestok-new.png?alt=media&token=2da76634-da68-45d3-ad03-5d69c34305e6'
  },
  {
    id: 3,
    name: 'Ашан',
    image: 'https://firebasestorage.googleapis.com/v0/b/groceryman-5b279.appspot.com/o/logos%2Fauchan.png?alt=media&token=1e7f7311-a42b-4105-a6f4-7684199445db'
  },
  {
    id: 4,
    name: 'Гипербола',
    image: 'https://firebasestorage.googleapis.com/v0/b/groceryman-5b279.appspot.com/o/logos%2Fgiperbola.jpg?alt=media&token=1ebb2d62-7fc7-4654-b55d-04772ee35037'
  },
  {
    id: 5,
    name: 'Карусель',
    image: 'https://firebasestorage.googleapis.com/v0/b/groceryman-5b279.appspot.com/o/logos%2Fkarusel.png?alt=media&token=05100d93-e027-402d-8196-537d8a2c9bf1'
  },
  {
    id: 6,
    name: 'Лента',
    image: 'https://firebasestorage.googleapis.com/v0/b/groceryman-5b279.appspot.com/o/logos%2Flenta.jpg?alt=media&token=4e081acb-ff58-4ffb-9036-42decec74322'
  },
  {
    id: 7,
    name: 'Магнит',
    image: 'https://firebasestorage.googleapis.com/v0/b/groceryman-5b279.appspot.com/o/logos%2Flogo-magnit.png?alt=media&token=5998267a-6083-41c2-b903-ac9d47d614b2'
  },
  {
    id: 8,
    name: 'О\'Кей',
    image: 'https://firebasestorage.googleapis.com/v0/b/groceryman-5b279.appspot.com/o/logos%2Flogo-okey.jpg?alt=media&token=dc7335fe-de52-4f72-8109-d5e45e7ecd31'
  },
  {
    id: 9,
    name: 'SPAR',
    image: 'https://firebasestorage.googleapis.com/v0/b/groceryman-5b279.appspot.com/o/logos%2Flogo-spar.jpg?alt=media&token=294ec12f-3553-43ba-8508-05fe7da517b5'
  },
  {
    id: 10,
    name: 'Пятерочка',
    image: 'https://firebasestorage.googleapis.com/v0/b/groceryman-5b279.appspot.com/o/logos%2Fpyatyorochka.png?alt=media&token=7247b94b-7c6e-4739-86fb-88cd25ff0abf'
  },
  {
    id: 11,
    name: 'Райт',
    image: 'https://firebasestorage.googleapis.com/v0/b/groceryman-5b279.appspot.com/o/logos%2Frait.png?alt=media&token=b22ee41e-4b52-4c01-b7ca-d062eab3da68'
  }
]

export default localshops

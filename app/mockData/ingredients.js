export const ingredients = [{
  'id': 0,
  'name': 'Спагетти'
}, {
  'id': 1,
  'name': 'Панчетта'
}, {
  'id': 2,
  'name': 'Твердый сыр'
}, {
  'id': 3,
  'name': 'Яичный желток'
}, {
  'id': 4,
  'name': 'Свежемолотый черный перец'
}, {
  'id': 5,
  'name': 'Соль'
}, {
  'id': 6,
  'name': 'Сахар'
}, {
  'id': 7,
  'name': 'Яйцо куриное'
}, {
  'id': 8,
  'name': 'Пшеничнапя мука'
}, {
  'id': 9,
  'name': 'Яблоко'
}, {
  'id': 10,
  'name': 'Растительное масло'
}, {
  'id': 11,
  'name': 'Сода'
}]

export const base = [
  {
    'name': 'Алкоголь',
    'image': '//s2.eda.ru/StaticContent/Photos/120213175433/141024150052/p_O.jpg'
  },
  {
    'name': 'Крупы, бобовые и мука',
    'image': '//s1.eda.ru/StaticContent/Photos/120213184037/141024151436/p_O.jpg'
  },
  {
    'name': 'Рыба и морепродукты',
    'image': '//s2.eda.ru/StaticContent/Photos/120213182322/141024150512/p_O.jpg'
  },
  {
    'name': 'Бакалея',
    'image': '//s2.eda.ru/StaticContent/Photos/120213175531/141024150112/p_O.jpg'
  },
  {
    'name': 'Специи и приправы',
    'image': '//s2.eda.ru/StaticContent/Photos/120213182735/141024150516/p_O.jpg'
  },
  {
    'name': 'Молочные продукты и яйца',
    'image': '//s2.eda.ru/StaticContent/Photos/120213180404/141024150452/p_O.jpg'
  },
  {
    'name': 'Сыры',
    'image': '//s2.eda.ru/StaticContent/Photos/120213183151/141024150519/p_O.jpg'
  },
  {
    'name': 'Овощи и корнеплоды',
    'image': '//s2.eda.ru/StaticContent/Photos/120213181135/141024150500/p_O.jpg'
  },
  {
    'name': 'Фрукты и ягоды',
    'image': '//s1.eda.ru/StaticContent/Photos/120213183611/141024150524/p_O.jpg'
  },
  {
    'name': 'Грибы',
    'image': '//s2.eda.ru/StaticContent/Photos/120213175727/141024150429/p_O.jpg'
  },
  {
    'name': 'Зелень и травы',
    'image': '//s1.eda.ru/StaticContent/Photos/120213180046/141024150435/p_O.jpg'
  },
  {
    'name': 'Мясо и мясная гастрономия',
    'image': '//s2.eda.ru/StaticContent/Photos/120213180745/141024150456/p_O.jpg'
  },
  {
    'name': 'Птица',
    'image': '//s1.eda.ru/StaticContent/Photos/120213181923/141024150508/p_O.jpg'
  },
  {
    'name': 'Орехи',
    'image': '//s2.eda.ru/StaticContent/Photos/120213181523/141024150505/p_O.jpg'
  },
  {
    'name': 'Готовые продукты',
    'image': '//s1.eda.ru/StaticContent/Photos/120213175629/141024150425/p_O.jpg'
  }
]

export default ingredients


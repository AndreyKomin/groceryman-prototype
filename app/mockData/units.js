const units = [ {
  'abbreviation': 'г.',
  'id': 0,
  'name': 'Грамм'
}, {
  'abbreviation': 'шт.',
  'id': 1,
  'name': 'Штук'
}, {
  'abbreviation': 'по вкусу',
  'id': 2,
  'name': 'По вкусу'
}, {
  'abbreviation': 'мл.',
  'id': 3,
  'name': 'Миллилитры'
}, {
  'abbreviation': 'ч./л.',
  'id': 4,
  'name': 'Чайная ложка'
}, {
  'abbreviation': 'с./л.',
  'id': 5,
  'name': 'Столовая ложка'
}, {
  'abbreviation': 'стакан',
  'id': 6,
  'name': 'Стакан'
} ]

export default units

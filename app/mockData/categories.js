const categories = [
  {
    'id': 1,
    'image': 'https://pp.userapi.com/c621706/v621706645/c163/zXkOYQozFLo.jpg',
    'name': 'Основные блюда',
    'count': 11495
  },
  {
    'id': 2,
    'image': 'https://pp.userapi.com/c621706/v621706645/c152/-2nIaqsgdiQ.jpg',
    'name': 'Выпечка и десерты',
    'count': 12844
  },
  {
    'id': 3,
    'image': 'https://pp.userapi.com/c621706/v621706645/c149/EgxFAmyWAyM.jpg',
    'name': 'Завтраки',
    'count': 2050
  },
  {
    'id': 4,
    'image': 'https://pp.userapi.com/c621706/v621706645/c197/aJzkVgmvlJo.jpg',
    'name': 'Супы',
    'count': 3118
  },
  {
    'id': 5,
    'image': 'https://pp.userapi.com/c621706/v621706645/c174/9AJo7EysXWY.jpg',
    'name': 'Салаты',
    'count': 5112
  },
  {
    'id': 6,
    'image': 'https://pp.userapi.com/c621706/v621706645/c1a1/pZ8NFI29_Uk.jpg',
    'name': 'Паста и пицца',
    'count': 1903
  },
  {
    'id': 7,
    'image': 'https://pp.userapi.com/c621706/v621706645/c17e/q8uCeZjA18E.jpg',
    'name': 'Сэндвичи',
    'count': 625
  },
  {
    'id': 8,
    'image': 'https://pp.userapi.com/c621706/v621706645/c190/DANm9ZmsMaQ.jpg',
    'name': 'Закуски',
    'count': 5472
  },
  {
    'id': 9,
    'image': 'https://pp.userapi.com/c621706/v621706645/c15c/BTnOLjfCrRQ.jpg',
    'name': 'Напитки',
    'count': 2093
  },
  {
    'id': 10,
    'image': 'https://pp.userapi.com/c621706/v621706645/c189/5DS09AT72UE.jpg',
    'name': 'Соусы и маринады',
    'count': 1339
  },
  {
    'id': 11,
    'image': 'https://pp.userapi.com/c621706/v621706645/c16d/20Do29gmmKA.jpg',
    'name': 'Ризотто',
    'count': 242
  }
]

export default categories
